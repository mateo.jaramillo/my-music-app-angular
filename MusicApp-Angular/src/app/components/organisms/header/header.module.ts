import { NgModule } from '@angular/core';
import { NavbarModule } from '../../molecules/navbar/navbar.module';
import { HeaderComponent } from './header.component';

@NgModule({
  declarations: [HeaderComponent],
  exports: [HeaderComponent],
  imports: [NavbarModule],
})
export class HeaderModule {}
