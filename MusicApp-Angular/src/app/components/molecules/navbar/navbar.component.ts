import { Component, OnInit } from '@angular/core';
import { MenuItem } from 'src/app/interfaces/menu-item';

@Component({
  selector: 'navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss'],
})
export class NavbarComponent implements OnInit {
  menuItems!: Array<MenuItem>;
  constructor() {}

  ngOnInit(): void {
    this.menuItems = [
      { name: 'Home', path: '/home', isActive: false },
      { name: 'Favorites', path: '/favorites', isActive: false },
      { name: 'Premium', path: '/premium', isActive: false },
    ];
  }
}
