import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { NavbarItemModule } from '../../atoms/navbar-item/navbar-item.module';
import { NavbarComponent } from './navbar.component';

@NgModule({
  declarations: [NavbarComponent],
  exports: [NavbarComponent],
  imports: [CommonModule, NavbarItemModule],
})
export class NavbarModule {}
