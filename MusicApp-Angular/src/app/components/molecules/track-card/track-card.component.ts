import { Component, Input, OnInit } from '@angular/core';
import { Track } from 'src/app/interfaces/track-data';
import { FavoritesService } from 'src/app/services/favorites.service';

@Component({
  selector: 'track-card',
  templateUrl: './track-card.component.html',
  styleUrls: ['./track-card.component.scss'],
})
export class TrackCardComponent implements OnInit {
  @Input() data!: Track;
  constructor(private favoritesService: FavoritesService) {}
  likePressed() {
    console.log(this.data);
    this.data.liked = !this.data.liked;
    if (this.data.liked === true) {
      this.favoritesService.addFavorite(this.data);
    } else {
      this.favoritesService.removeFavorite(this.data);
    }
  }
  ngOnInit(): void {}
}
