import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TrackCardComponent } from './track-card.component';
import { TrackDataModule } from '../../atoms/track-data/track-data.module';
import { TrackImageModule } from '../../atoms/track-image/track-image.module';

@NgModule({
  declarations: [TrackCardComponent],
  exports: [TrackCardComponent],
  imports: [CommonModule, TrackDataModule, TrackImageModule],
})
export class TrackCardModule {}
