import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TrackDataComponent } from './track-data.component';

@NgModule({
  declarations: [TrackDataComponent],
  exports: [TrackDataComponent],
  imports: [CommonModule],
})
export class TrackDataModule {}
