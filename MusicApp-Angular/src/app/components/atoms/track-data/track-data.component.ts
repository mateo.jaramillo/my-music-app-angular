import { Component, Input, OnInit } from '@angular/core';
import { Track } from 'src/app/interfaces/track-data';

@Component({
  selector: 'track-data',
  templateUrl: './track-data.component.html',
  styleUrls: ['./track-data.component.scss'],
})
export class TrackDataComponent implements OnInit {
  @Input() data?: Track;
  constructor() {}

  ngOnInit(): void {}
}
