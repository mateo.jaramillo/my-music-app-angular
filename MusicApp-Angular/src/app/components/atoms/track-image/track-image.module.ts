import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TrackImageComponent } from './track-image.component';

@NgModule({
  declarations: [TrackImageComponent],
  exports: [TrackImageComponent],
  imports: [CommonModule],
})
export class TrackImageModule {}
