import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'track-image',
  templateUrl: './track-image.component.html',
  styleUrls: ['./track-image.component.scss'],
})
export class TrackImageComponent implements OnInit {
  @Input() image: any;
  constructor() {}

  ngOnInit(): void {}
}
