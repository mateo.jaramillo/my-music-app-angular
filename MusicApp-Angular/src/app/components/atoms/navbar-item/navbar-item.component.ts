import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'navbar-item',
  templateUrl: './navbar-item.component.html',
  styleUrls: ['./navbar-item.component.scss'],
})
export class NavbarItemComponent implements OnInit {
  @Input() title!: string;
  @Input() path!: string;

  constructor() {}

  ngOnInit(): void {}
}
