import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { NavbarItemComponent } from './navbar-item.component';

@NgModule({
  declarations: [NavbarItemComponent],
  exports: [NavbarItemComponent],
  imports: [CommonModule],
})
export class NavbarItemModule {}
