import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HomeComponent } from './home.component';
import { HeaderModule } from 'src/app/components/organisms/header/header.module';
import { HttpClientModule } from '@angular/common/http';
import { TrackCardModule } from 'src/app/components/molecules/track-card/track-card.module';

@NgModule({
  declarations: [HomeComponent],
  exports: [HomeComponent],
  imports: [CommonModule, HeaderModule, TrackCardModule],
  providers: [HttpClientModule],
})
export class HomeModule {}
