import { Component, OnInit } from '@angular/core';
import { Item, SpotifyResponse } from 'src/app/interfaces/spotify-data';
import { SpotifyToken } from 'src/app/interfaces/spotify-token';
import { SpotifyService } from 'src/app/services/spotify.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
})
export class HomeComponent implements OnInit {
  // token?: string;
  songs!: Item[];
  constructor(private spotifyService: SpotifyService) {}

  ngOnInit(): void {
    this.spotifyService.getToken().subscribe((data: SpotifyToken) => {
      console.log(data);
      // this.token = data.access_token;
      localStorage.setItem('access_token', data.access_token);
    });
    this.spotifyService.getMusic().subscribe((data: SpotifyResponse) => {
      this.songs = data.albums.items;
    });
  }
}
