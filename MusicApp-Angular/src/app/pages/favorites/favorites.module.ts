import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FavoritesComponent } from './favorites.component';
import { TrackCardModule } from 'src/app/components/molecules/track-card/track-card.module';

@NgModule({
  declarations: [FavoritesComponent],
  exports: [FavoritesComponent],
  imports: [CommonModule, TrackCardModule],
})
export class FavoritesModule {}
