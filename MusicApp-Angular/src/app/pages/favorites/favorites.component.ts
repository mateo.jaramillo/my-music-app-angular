import { Component, OnInit } from '@angular/core';
import { Track } from 'src/app/interfaces/track-data';
import { FavoritesService } from 'src/app/services/favorites.service';

@Component({
  selector: 'favorites',
  templateUrl: './favorites.component.html',
  styleUrls: ['./favorites.component.scss'],
})
export class FavoritesComponent implements OnInit {
  tracks: Track[] = [];
  // tracks: any = [];
  constructor(private favoritesService: FavoritesService) {}

  ngOnInit(): void {
    this.tracks = this.favoritesService.getFavorites();
    console.log(this.tracks);
  }
}
