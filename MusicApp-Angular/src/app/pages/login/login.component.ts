import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { initializeApp } from 'firebase/app';
import { FirebaseService } from 'src/app/services/firebase.service';

@Component({
  selector: 'login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent implements OnInit {
  isLogin!: boolean;
  email: string = '';
  password: string = '';
  @Output() outputData: EventEmitter<any> = new EventEmitter();
  // El tipo de dato que quiero que se emita lo puedo controlar con una interface
  // Este event emitter se comporta como un observable al cual me puedo suscribir
  // Pero la función principal es en base al método emit(data)

  login() {
    this.firebase.logIn(this.email, this.password);
  }
  newUser() {
    this.firebase.register(this.email, this.password);
  }
  changeForm() {
    this.isLogin = !this.isLogin;
  }

  constructor(private firebase: FirebaseService) {}

  ngOnInit(): void {
    this.isLogin = true;
  }
}
