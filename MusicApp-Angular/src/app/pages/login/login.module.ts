import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { LoginComponent } from './login.component';

@NgModule({
  declarations: [LoginComponent],
  imports: [
    CommonModule,
    FormsModule, // Se importa porque lo usaré para gestionar los eventos del formulario
  ],
  exports: [LoginComponent],
})
export class LoginModule {}
