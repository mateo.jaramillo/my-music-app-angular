import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NavbarModule } from './components/molecules/navbar/navbar.module';
import { FavoritesModule } from './pages/favorites/favorites.module';
import { HomeModule } from './pages/home/home.module';
import { LoginModule } from './pages/login/login.module';
@NgModule({
  declarations: [AppComponent],
  imports: [
    BrowserModule,
    AppRoutingModule,
    NavbarModule,
    LoginModule,
    HomeModule,
    HttpClientModule,
    FavoritesModule,
  ],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
