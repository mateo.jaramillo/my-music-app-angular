import { Injectable } from '@angular/core';
import { Track } from '../interfaces/track-data';

@Injectable({
  providedIn: 'root',
})
export class FavoritesService {
  private _favorites: Track[] = [];
  addFavorite(track: Track) {
    // this._favorites.push(track);//Lo agrega al final
    this._favorites.unshift(track); // Lo agrega al principio
  }
  removeFavorite(track: Track) {
    this._favorites;
  }
  getFavorites() {
    return [...this._favorites]; //Para romper la referencia, se manda una copia con el operador spread
  }
}
