import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { SpotifyResponse } from '../interfaces/spotify-data';
import { SpotifyToken } from '../interfaces/spotify-token';

@Injectable({
  providedIn: 'root',
})
export class SpotifyService {
  constructor(private http: HttpClient) {}

  client_id = '910bb4e45cd44aacaca07eea2bc5e61a';
  client_secret = '00095c77787448a9b9cce19b11aa712a';

  getToken() {
    const authorizationTokenUrl = `https://accounts.spotify.com/api/token`;
    const body = 'grant_type=client_credentials';
    return this.http.post<SpotifyToken>(authorizationTokenUrl, body, {
      headers: new HttpHeaders({
        Authorization:
          'Basic  ' + btoa(this.client_id + ':' + this.client_secret),
        'Content-Type': 'application/x-www-form-urlencoded;',
      }),
    });
  }

  getMusic() {
    const musicUrl = 'https://api.spotify.com/v1/browse/new-releases';

    return this.http.get<SpotifyResponse>(musicUrl, {
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${localStorage.getItem('access_token')}`,
      },
    });
  }
}
