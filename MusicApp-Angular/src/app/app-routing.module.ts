import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './pages/login/login.component';
import { HomeComponent } from './pages/home/home.component';
import { FavoritesComponent } from './pages/favorites/favorites.component';
const routees: Routes = [
  {
    path: '',
    component: HomeComponent,
    children: [
      // {path: 'recents', component: ''}
      { path: 'favorites', component: FavoritesComponent },
    ],
  },
  // {
  //   path: 'login',
  //   component: LoginComponent,
  //   pathMatch: 'full',
  // },
  // {
  //   path: 'home',
  //   component: HomeComponent,
  //   pathMatch: 'full',
  // },
  // {
  //   path: 'favorites',
  //   component: FavoritesComponent,
  //   pathMatch: 'full',
  // },
  // {
  //   path: '**',
  //   redirectTo: '/404',
  // },
];

@NgModule({
  imports: [RouterModule.forRoot(routees)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
