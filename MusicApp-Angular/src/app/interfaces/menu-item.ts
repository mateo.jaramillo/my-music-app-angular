export interface MenuItem {
  name: string;
  path: string;
  isActive: boolean;
}
