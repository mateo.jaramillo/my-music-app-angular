export interface Track {
  title: string;
  artist: string;
  album: string;
  image: string;
  liked: boolean;
}
